'use strict';

module.exports = function(grunt) {

    grunt.initConfig({
        cssmin: {
            bundle: {
                files: {
                    './dist/main.min.css': './app/css/main.css'
                }
            }
        },
        uglify: {
            bundle: {
                files: {
                    './dist/main.min.js': [
                        './app/core/treelookup.js',
                        './app/core/treelookup-bfs.js',
                        './app/scripts/utils.js',
                        './app/scripts/index.js'
                    ]
                }
            }
        },
        htmlbuild: {
            dist: {
                src: 'app/index.html',
                dest: 'dist/',
                options: {
                    scripts: {
                        bundle: [ './dist/*.min.js' ]
                    },
                    styles: {
                        bundle: [ './dist/*.min.css' ]
                    }
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-html-build');

    grunt.registerTask('build', ['cssmin:bundle', 'uglify:bundle', 'htmlbuild:dist']);

};