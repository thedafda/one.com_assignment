'use strict';

(function(win, doc) {

    var appUtils = win.appUtils;
    var TreeLookup = win.TreeLookup;
    var TreeBfsSearch = win.TreeBfsSearch.TreeBfsSearch;

    function TreeBfsSearchUIComponent(rootEle) {
        this.tree = new TreeLookup();
        this.bfsSearch = new TreeBfsSearch(this.tree);
        this.searchInput = rootEle.querySelector('[data-search-input]');
        this.searchError = rootEle.querySelector('[data-search-error]');
        this.searchNoResult = rootEle.querySelector('[data-search-no-result]');
        this.searchResult = rootEle.querySelector('[data-search-result]');
        this.bindEvents();
    }

    TreeBfsSearchUIComponent.prototype.bindEvents = function() {
        this.onSearchInputKeyupCb = this.onSearchInput.bind(this);
        this.searchInput.addEventListener('keyup', this.onSearchInputKeyupCb);
    };

    TreeBfsSearchUIComponent.prototype.onSearchInput = function(evt) {
        var value = evt.target.value;

        this.setError('')
        this.setNoResult(false);

        if (!appUtils.isNumeric(value)) {
            this.setError('Only Numeric input is allowed')
            return;
        }

        this.bfsSearch.search(value).then(function(data) {
            var result = data.map(function(o) {
                return '<div>' + o.join(' / ') + '</div>';
            }).join('');
            this.setResult(result);
        }.bind(this));
    };

    TreeBfsSearchUIComponent.prototype.setError = function(errorMsg) {
        this.searchError.textContent = errorMsg;
    };

    TreeBfsSearchUIComponent.prototype.setNoResult = function(flag) {
        this.searchNoResult.textContent = flag ? 'No Results Found' : '';
    };

    TreeBfsSearchUIComponent.prototype.setResult = function(html) {
        this.searchResult.innerHTML = html;
    };

    TreeBfsSearchUIComponent.prototype.onDestroy = function() {
        this.searchInput.removeEventListener('keyup', this.onSearchInputKeyupCb);
    };

    function init() {
        var rootEle = doc.getElementById("root");
        var tbs = new TreeBfsSearchUIComponent(rootEle);
    }

    win.appUtils.onDocumentReady(init);

})(window, window.document);