'use strict';

(function(win, doc) {

	function isNumeric(num) {
		return !isNaN(num);
	}

	function onDocumentReady(callback) {
		doc.addEventListener("DOMContentLoaded", callback);
	}

	win.appUtils = {
		isNumeric: isNumeric,
		onDocumentReady: onDocumentReady
	};

})(window, window.document);
