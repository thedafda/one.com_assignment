# One.com Assignment #

[ ![Codeship Status for thedafda/one.com_assignment](https://app.codeship.com/projects/3889c690-5b80-0135-60af-5a18e193cc36/status?branch=master)](https://app.codeship.com/projects/237575)

### Tree BFS Search Implementations Production Build Links ###

* VanillaJS Implementation
	- https://s3.amazonaws.com/jd-assignments/vanilla/index.html
* Angular4 Implementation
	- https://s3.amazonaws.com/jd-assignments/angular/index.html
* ReactJS Implementation
	- https://s3.amazonaws.com/jd-assignments/react/index.html



### Dev Requirements ###
* NodeJS 6.11+


### Directory Structure And Commands ###

`core` : contains for core engine for BFS search of tree, with mocha unit tests for basic scenario

* Commands
	- Run Test: `npm test`


`react` : constains React implementation for BFS search using core

* Commands
	- Run Server: `npm start`
	- Build Production: `npm run build`


`angular` : constains Angular4+ implementation for BFS search using core

* Commands
	- Run Server: `npm start`
	- Build Production: `npm run build`


`vanilla` : constains VanillaJS implementation without any third-party tools using core

* Commands
	- Run Server: `npm start`
	- Build Production: `npm run build`, custom `grunt` build used for the project

### Commands ###
* `sh setup.sh` - to install all depedencies and make production build for all implementations

### CI/CD ###
* CodeShip(https://codeship.com/) is used to build, test and deploy code to S3


