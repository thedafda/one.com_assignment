import React, { Component } from 'react';
import './App.css';

import TreeLookup from './core/treelookup';
import { TreeBfsSearch } from './core/treelookup-bfs';

class App extends Component {

  constructor() {
    super();
    this.state = {
      error: null,
      result: null
    };

    this.tree = new TreeLookup();
    this.bfsSearch =  new TreeBfsSearch(this.tree);
  }

  onKeyUp(evt) {

    this.setState({
      error: null,
      result: null
    });

    const { value } = evt.target;

    if (!this.isNumeric(value)) {
        this.setState({
          error: 'Only Numeric input is allowed'
        });
        return;
    }

    this.bfsSearch.search(value).then((data) => {
      this.setState({
        result: data
      });
    });
  }

  isNumeric(num) {
    return !isNaN(num);
  }

  render() {

    const { error, result } = this.state;
    return (
      <div>
        <h2 className="title">ReactJS Tree BFS Search Implementation</h2>
        <div className="tree-search">
          <div className="input-cont">
            <input onKeyUp={this.onKeyUp.bind(this)} placeholder="Number to search" />
          </div>
          <div className="search-result">
            {error && (
              <div className="error">
                {error}
              </div>
            )}
            {result && result.length === 0 && (
              <div className="no-result">
                No Results Found
              </div>
            )}
            {result && result.length !== 0 && (
              <div className="result">
                {result.map((r, idx) => {
                  return (
                    <div key={idx}>
                      {r.join(' / ')}
                    </div>
                  );
                })}
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
