'use strict';

const assert = require('assert');

const { TreeBfsSearch } = require('../treelookup-bfs');
const TreeLookup = require('../treelookup');


describe('Tree BFS Search', function(){
    

    it('should return all 4 paths for node 8', function(done){

    	const tree = new TreeLookup();
    	const bfsSearch = new TreeBfsSearch(tree);

    	const node = '8';

    	bfsSearch
    		.search(node)
    		.then((paths) => {
    			assert.equal(paths.length, 4);
    			paths.forEach((p) => {
    				assert.equal(p[p.length - 1], node);
    			});
    			done();
    		})
    		.catch(() => {
    			done("Error While Searching " + node);
    		});
        
    });

    it('should return empty array for non existent node', function(done){

    	const tree = new TreeLookup();
    	const bfsSearch = new TreeBfsSearch(tree);

    	const node = '99';

    	bfsSearch
    		.search(node)
    		.then((paths) => {
    			assert.equal(paths.length, 0);
    			done();
    		})
    		.catch(() => {
    			done("Error While Searching " + node);
    		});

    });

});
