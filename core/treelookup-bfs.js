'use strict';

(function(exports) {
     
    function findNumberInTree(treeLookup, num) {
        var fnum = num + '';
        return treeLookup.getChildrenAsPromise('/').then(function(d) {
            var pathCollector = [];
            return check(treeLookup, '/', d, fnum, pathCollector).then(function() {
                return pathCollector;
            });
        });
    }


    function check(treeLookup, base, paths, number, pathCollector) {

        if (paths && paths.length === 0) {
            return Promise.resolve();
        }

        var found = paths.find(function(o) {
            return o === number;
        });
        if (found) {
            var final = base.split('/').filter(function(o) { return o; }).concat(found);
            pathCollector.push(final);
            return Promise.resolve();
        }
            
        var pArr = paths.map(function(o) {
            return treeLookup.getChildrenAsPromise(base + o)
                .then(function(data) {
                    return {
                        base: base + o,
                        paths: data
                    };
                });
        });
        
        return Promise.all(pArr)
            .then(function(arr) {
                var pxr = arr.map(function(data) {
                    var paths = data.paths;
                    var base = data.base + '/';
                    return check(treeLookup, base, paths, number, pathCollector);
                });
                return Promise.all(pxr);
            });
        
    }

    function TreeBfsSearch(treeLookup) {
        this.treeLookup = treeLookup;
    }


    TreeBfsSearch.prototype.search = function(num) {
        return findNumberInTree(this.treeLookup, num);
    };

    exports.TreeBfsSearch = TreeBfsSearch;

})(typeof exports === 'undefined' ? this['TreeBfsSearch']={} : exports);
