import { Component } from '@angular/core';

import TreeLookup from '../../../core/treelookup';
import { TreeBfsSearch } from '../../../core/treelookup-bfs';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {

    searchStr: string;

    tree: any;
    bfsSearch: any; 

    result: any;

    error: string;

    constructor() {
        this.tree = new TreeLookup();
        this.bfsSearch =  new TreeBfsSearch(this.tree);
    }

    onKeyUp(evt) {
        this.error = undefined;
        this.result = undefined;
        if (!this.isNumeric(this.searchStr)) {
            this.error = 'Only Numeric input is allowed';
            return;
        }

        this.bfsSearch
            .search(this.searchStr)
            .then((data) => {
                this.result = data;
            });
    }

    isNumeric(num) {
        return !isNaN(num);
    }

}
