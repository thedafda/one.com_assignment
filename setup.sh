echo "Installing deps for Code Tree BSF project"
cd core
npm install

echo "Testing Core"
npm test
cd ..

echo "--------------------------"

echo "Installing deps for Angular4 project"
cd angular
npm install
echo "Generating production build for Angular4"
npm run codeship-build
cd ..

echo "-------------------------"

echo "Installing deps for ReactJS project"
cd reactjs
npm install
echo "Generating production build for ReactJS"
npm run build
cd ..

echo "-------------------------"

echo "Installing deps for VanillaJS project"
cd vanilla
npm install
echo "Generating production build for VanillaJS"
npm run build
cd ..
